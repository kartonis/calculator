#pragma ones
#include <iostream>
#include <string>
#include <stack>
#include <fstream>
#include <map>
#include <vector>
#include <cmath>
#include <stdexcept>
class Set
{
    friend class Add;
    friend class Sub;
    friend class Mul;
    friend class Div;
    friend class Sqrt;
    std::stack <double> Stack;
    std::map <std::string, double> Map;
public:
    void selector(std::vector<std::string>);
};
std::vector <std::string> split(std::string expression);
class Calculation
{
public:
    virtual void count(Set &ob)=0;
};

class Add : public Calculation
{
public:
    void count(Set &ob);
};
class Sub : public Calculation
{
public:
    void count(Set &ob);
};
class Mul : public Calculation
{
public:
    void count(Set &ob);
};
class Div : public Calculation
{
public:
    void count(Set &ob);
};
class Sqrt : public Calculation
{
public:
    void count(Set &ob);
};

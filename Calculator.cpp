#include "Calculator.h"
std::vector <std::string> split(std::string expression) {
    std::vector<std::string> word;
    while (expression.find(' ') != expression.npos) {
        word.push_back(expression.substr(0, expression.find(' ')));
        expression = expression.substr(expression.find(' ')+1, expression.size());
    }
    word.push_back(expression);
    if(word.size()>3)
        throw std::invalid_argument("Too long expression");
    return word;
}
void Add::count(Set &ob)
{
    double operand1=ob.Stack.top();
    ob.Stack.pop();
    double operand2=ob.Stack.top();
    ob.Stack.pop();
    ob.Stack.push(operand1+operand2);
}
void Sub::count(Set &ob)
{
    double operand1=ob.Stack.top();
    ob.Stack.pop();
    double operand2=ob.Stack.top();
    ob.Stack.pop();
    ob.Stack.push(operand2-operand1);
}
void Mul::count(Set &ob)
{
    double operand1=ob.Stack.top();
    ob.Stack.pop();
    double operand2=ob.Stack.top();
    ob.Stack.pop();
    ob.Stack.push(operand1*operand2);
}
void Div::count(Set &ob)
{
    double operand1=ob.Stack.top();
    ob.Stack.pop();
    double operand2=ob.Stack.top();
    ob.Stack.pop();
    if(operand1==0)
        throw std::invalid_argument("Division by zero");
    ob.Stack.push(operand2/operand1);
}
void Sqrt::count(Set &ob)
{
    double operand=ob.Stack.top();
    ob.Stack.pop();
    if(operand<0)
        throw std::invalid_argument("Negative argument");
    ob.Stack.push(sqrt(operand));
}
void Set::selector(std::vector<std::string> arguments)
{
    if (arguments[0]=="PUSH")
    {
        auto it=Map.find(arguments[1]);
        double num;
        if(it!=Map.end())
            Stack.push(it->second);
        else {
            num = std::stod(arguments[1]);
            Stack.push(num);
        }
    }
    else if (arguments[0]=="DEFINE")
    {
        double num=std::stod(arguments[2]);
        Map.insert_or_assign(arguments[1],num);
    }
    else if(arguments[0]=="POP")
    {
        if(Stack.size()==0)
            throw std::invalid_argument("Stack is empty");
        Stack.pop();
    }
    else if(arguments[0]=="PRINT")
    {
        if(Stack.size()==0)
            throw std::invalid_argument("Stack is empty");
        std::cout << Stack.top()<<std::endl;
    }
    else if(arguments[0]=="+")
    {
        Add operation;
        operation.count(*this);
    }
    else if (arguments[0]=="-")
    {
        Sub operation;
        operation.count(*this);
    }
    else if (arguments[0]=="*")
    {
        Mul operation;
        operation.count(*this);
    }
    else if (arguments[0]=="/")
    {
        try {
            Div operation;
            operation.count(*this);
        }
        catch(std::invalid_argument &e) {
            throw;
        }
    }
    else if (arguments[0]=="SQRT") {
        try {
            Sqrt operation;
            operation.count(*this);
        }
        catch(std::invalid_argument &e){
            throw;
        }
    }
    else
    {
        throw std::invalid_argument("Not a command");
    }
}
#include "gtest/gtest.h"
#include "Calculator.h"
#include <sstream>

TEST(Check_Push_Print, Case1)
{
    Set ob;
    ob.selector({"PUSH", "4"});
    std::ostringstream os;
    std::streambuf *pointer=std::cout.rdbuf(os.rdbuf());
    ob.selector({"PRINT"});
    EXPECT_EQ(os.str(),"4\n");
}
TEST(Check_Pop, Case2)
{
    Set ob;
    std::ostringstream os;
    std::streambuf *pointer=std::cout.rdbuf(os.rdbuf());
    ob.selector({"PUSH", "2"});
    ob.selector({"PUSH", "7"});
    ob.selector({"PRINT"});
    ob.selector({"POP"});
    ob.selector({"PRINT"});
    EXPECT_EQ(os.str(), "7\n2\n");
}
TEST(Check_Define, Case3)
{
    Set ob;
    std::ostringstream os;
    std::streambuf *pointer=std::cout.rdbuf(os.rdbuf());
    ob.selector({"DEFINE", "pi", "3.14"});
    ob.selector({"PUSH", "pi"});
    ob.selector({"PRINT"});
    EXPECT_EQ(os.str(), "3.14\n");
}
TEST(Check_Add, Case4)
{
    Set ob;
    std::ostringstream os;
    std::streambuf *pointer=std::cout.rdbuf(os.rdbuf());
    ob.selector({"PUSH", "6"});
    ob.selector({"PUSH", "1"});
    ob.selector({"+"});
    ob.selector({"PRINT"});
    EXPECT_EQ(os.str(), "7\n");
}
TEST(Check_Sub, Case5)
{
    Set ob;
    std::ostringstream os;
    std::streambuf *pointer=std::cout.rdbuf(os.rdbuf());
    ob.selector({"PUSH", "4"});
    ob.selector({"PUSH", "5"});
    ob.selector({"-"});
    ob.selector({"PRINT"});
    EXPECT_EQ(os.str(), "-1\n");
}
TEST(Check_Mul, Case5)
{
    Set ob;
    std::ostringstream os;
    std::streambuf *pointer=std::cout.rdbuf(os.rdbuf());
    ob.selector({"PUSH", "6"});
    ob.selector({"PUSH", "0"});
    ob.selector({"*"});
    ob.selector({"PRINT"});
    EXPECT_EQ(os.str(), "0\n");
}
TEST(Check_Div, Case6)
{
    Set ob;
    std::ostringstream os;
    std::streambuf *pointer=std::cout.rdbuf(os.rdbuf());
    ob.selector({"PUSH", "69"});
    ob.selector({"PUSH", "3"});
    ob.selector({"/"});
    ob.selector({"PRINT"});
    EXPECT_EQ(os.str(), "23\n");
}
TEST(Check_Sqrt, Case7)
{
    Set ob;
    std::ostringstream os;
    std::streambuf *pointer=std::cout.rdbuf(os.rdbuf());
    ob.selector({"PUSH", "144"});
    ob.selector({"SQRT"});
    ob.selector({"PRINT"});
    EXPECT_EQ(os.str(), "12\n");
}
TEST(Negative_argument, Case8)
{
    Set ob;
    std::ostringstream os;
    std::streambuf *pointer=std::cout.rdbuf(os.rdbuf());
    try
    {
        ob.selector({"PUSH", "-1"});
        ob.selector({"SQRT"});
        ob.selector({"PRINT"});
        EXPECT_EQ(1,0);
    }
    catch(const std::invalid_argument &e)
    {
        EXPECT_EQ(1,1);
    }
}
TEST(Division_by_zero, Case9)
{
    Set ob;
    std::ostringstream os;
    std::streambuf *pointer=std::cout.rdbuf(os.rdbuf());
    try
    {
        ob.selector({"PUSH", "5"});
        ob.selector({"PUSH", "0"});
        ob.selector({"/"});
        ob.selector({"PRINT"});
        EXPECT_EQ(1,0);
    }
    catch(const std::invalid_argument &e)
    {
        EXPECT_EQ(1,1);
    }
}
TEST(Stack_is_empty, Case10)
{
    Set ob;
    std::ostringstream os;
    std::streambuf *pointer=std::cout.rdbuf(os.rdbuf());
    try
    {
        ob.selector({"PRINT"});
        EXPECT_EQ(1,0);
    }
    catch(const std::invalid_argument &e)
    {
        EXPECT_EQ(1,1);
    }
}
TEST(Vector, Case11)
{
    Set ob;
    std::ostringstream os;
    std::streambuf *pointer=std::cout.rdbuf(os.rdbuf());
    try
    {
        ob.selector({"DEFINE", "a", "b", "5"});
        EXPECT_EQ(0,1);
    }
    catch(const std::invalid_argument &e)
    {
        EXPECT_EQ(1,1);
    }
}
int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();
    return 0;
}
#include "Calculator.h"
int main(int argc, char ** argv) {
    std::string expression;
    Set ob;
    std::vector<std::string> word;
    std::ifstream in;
    if (argc==2) {
        in.open(argv[1]);
        std::cin.rdbuf(in.rdbuf());
    }
    while (std::cin.good()) {
        getline(std::cin, expression);
        if (!expression.empty()) {
            if (expression[0] != '#') {
                try {
                    word = split(expression);
                }
                catch (const std::invalid_argument &e) {
                    std::cerr << "You have a problem " << e.what() << std::endl;
                }
                try {
                    ob.selector(word);
                }
                catch (std::invalid_argument &e) {
                    std::cerr << "You have a problem " << e.what() << std::endl;
                }
            }
        }
    }
    if(in)
    {
        in.close();
    }
    return 0;
}